﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMMClient
{
    public class Encrypter
    {
        public static String doEncryptAES(String plainText, String key)
        {
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, getRijndaelManaged(key)));
        }



        public static String doEncryptAES(String plainText, byte[] key)
        {
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, getRijndaelManaged(key)));
        }

        public static String doDecryptAES(String encryptedText, String key)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return Encoding.UTF8.GetString(Decrypt(encryptedBytes, getRijndaelManaged(key)));
        }

        private static RijndaelManaged getRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 256,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        private static RijndaelManaged getRijndaelManaged(byte[] secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = secretKey;
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 256,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        private static byte[] Encrypt(byte[] plainBytes, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        private static byte[] Decrypt(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }

        public static byte[] GenerateAESKey()
        {
            AesCryptoServiceProvider crypto = new AesCryptoServiceProvider();
            crypto.KeySize = 256;
            crypto.BlockSize = 128;
            crypto.GenerateKey();
            byte[] keyGenerated = crypto.Key;
            return keyGenerated;
        }


        static public string Encryption(byte[] Data, string publicKey)
        {
            try
            {
                byte[] encryptedData;
                RSA rsa = RSA.Create();
                rsa.KeySize = 2048;
                RSAParameters RSAKeyInfo = new RSAParameters();
                //byte[] pembyte = File.ReadAllBytes(AppDomain.CurrentDomain.BaseDirectory + "publickey.pem");
                //var x506 = new X509Certificate2(pembyte);
                //var rsa2 = x506.GetRSAPublicKey();
                //encryptedData = rsa2.Encrypt(Data, RSAEncryptionPadding.Pkcs1);
                //Set RSAKeyInfo to the public key values.
                RSAKeyInfo.Modulus = Convert.FromBase64String(publicKey);
                byte[] Exponent = { 1, 0, 1 };
                RSAKeyInfo.Exponent = Exponent;
                rsa.ImportParameters(RSAKeyInfo);
                encryptedData = rsa.Encrypt(Data, RSAEncryptionPadding.Pkcs1);


                //using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(2048))
                //{
                //    RSAParameters RSAKeyInfo = new RSAParameters();

                //    //Set RSAKeyInfo to the public key values.
                //    RSAKeyInfo.Modulus = Convert.FromBase64String(publicKey);
                //    byte[] Exponent = { 1, 0, 1 };
                //    RSAKeyInfo.Exponent = Exponent;
                //    //Import key parameters into RSA.
                //    RSA.ImportParameters(RSAKeyInfo);

                //    encryptedData = RSA.Encrypt(Data,false);
                //}
                string Key = Convert.ToBase64String(encryptedData);
                return Key;
                
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        

        

        //Not Required
        static public byte[] Decryption(byte[] Data, RSAParameters RSAKey, bool DoOAEPPadding)
        {
            try
            {
                byte[] decryptedData;
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    RSA.ImportParameters(RSAKey);
                    decryptedData = RSA.Decrypt(Data, DoOAEPPadding);
                }
                return decryptedData;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public static string DecryptFromExternalSourcce(string cypherText, string key)
        {
            var bytesToEncrypt = Convert.FromBase64String(cypherText);
            //var bytesToEncrypt = Encoding.UTF8.GetBytes(cypherText);
            using (var rsa = new RSACryptoServiceProvider())
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(RSAParameters));
                    using (TextReader reader = new StringReader(key))
                    {
                        RSAParameters result = (RSAParameters)serializer.Deserialize(reader);
                        rsa.ImportParameters(result);
                    }

                    // rsa.FromXmlString(key);

                    var encryptedData = rsa.Decrypt(bytesToEncrypt, RSAEncryptionPadding.Pkcs1);
                    var base64Encrypted = Encoding.UTF8.GetString(encryptedData);
                    var b4 = Convert.ToBase64String(encryptedData);
                    // var base64Encrypted = Encoding.UTF8.GetString(encryptedData);
                    return b4;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }

        

    }

}
