﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace DMMClient
{
    class Program
    {
        
        static string token;
        static void Main(string[] args)
        {
            
            token = getToken();

            var s = retrieveClaim();

        }

        public static string retrieveClaim()
        {
            HttpResponseMessage request;
            try
            {

                string aesKey = "nsl7BTkDKyJdh6O055yPDAYAUJgFvi1Bvm904fSJg5I=";
                var aesKeyByte = Convert.FromBase64String(aesKey);

                string postBody = "{\"accountNumber\": \"3270006670\",\"affiliateCode\": \"ENG\",\"claimAmount\": 10000,\"currency\": \"NGN\",\"endDate\": \"01/01/2020\",\"startDate\": \"01/01/2018\"}";
                string bb = AesCng.encrpt(postBody, aesKeyByte);

                using (var client = new HttpClient())
                {
                    //Define Headers
                    ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => { return true; };
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("x-client-id", "ejNqZS9pMkljMFg5QnR1QlRVL05STUpJcUlDTTg0L3dJQT09OjpHc0l3SEF3elhHUnZGb0hOOjp3SFp0Ylkwbm1aaCt0WTlWL3YvODlRdEloUG8zdFhFK0VyUTRoK0lwbmxrPQ==");
                    client.DefaultRequestHeaders.Add("x-client-secret", "e121a474b41f8b126b45b04439a8c74e15fb2c438ca8ce36e72bc4b2ec672bfe9429b10713e4ecc0f2fa14d39388e1ae208f95e2ef97707a1315924ec01c2aa5");
                    client.DefaultRequestHeaders.Add("x-source-code", "CRMCLIENT");
                    client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
                    var data = new StringContent(bb, Encoding.UTF8, "text/plain");
                    client.Timeout = new TimeSpan(0, 2, 0);

                    string url = "https://appsuat.ecobank.com/dmmservice/api/v2/claims/retrieve";
                    request = client.PostAsync(url, data).Result;
                    if (request.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {

                    }
                    var response = request.Content.ReadAsStringAsync().Result;
                    var responseHeader = request.Headers.GetValues("x-secret-key").FirstOrDefault();

                    var r = AesCng.decrypt(response, aesKey);
                    return r;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getToken()
        {
            HttpResponseMessage request;
            string url = "https://appsuat.ecobank.com/dmmservice/api/v2/auth/token";
            using (var client = new HttpClient())
            {
                //Define Headers
                ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => { return true; };
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("x-client-id", "ejNqZS9pMkljMFg5QnR1QlRVL05STUpJcUlDTTg0L3dJQT09OjpHc0l3SEF3elhHUnZGb0hOOjp3SFp0Ylkwbm1aaCt0WTlWL3YvODlRdEloUG8zdFhFK0VyUTRoK0lwbmxrPQ==");
                client.DefaultRequestHeaders.Add("x-client-secret", "e121a474b41f8b126b45b04439a8c74e15fb2c438ca8ce36e72bc4b2ec672bfe9429b10713e4ecc0f2fa14d39388e1ae208f95e2ef97707a1315924ec01c2aa5");
                client.DefaultRequestHeaders.Add("x-source-code", "CRMCLIENT");
                client.Timeout = new TimeSpan(0, 2, 0);
                
                request = client.GetAsync(url).Result;
                if (request.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {

                }
                var response = request.Content.ReadAsStringAsync().Result;
                var res = JsonConvert.DeserializeObject<Token>(response);
                return res.Data.AccessToken;
            }
        }

    }
}
