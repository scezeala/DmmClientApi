﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Security.Cryptography;

namespace DMMClient
{
    public class AesCng
    {
        public static string encrpt(string textToEncrypt, byte[] key)
        {
            // Console.WriteLine("Hello World!");
            using (AuthenticatedAesCng aes = new AuthenticatedAesCng())
            {
                // Setup an authenticated chaining mode - The two current CNG options are

                // CngChainingMode.Gcm and CngChainingMode.Ccm. This should be done before setting up

                // the other properties, since changing the chaining mode can update things such as the

                // valid and current tag sizes.

                aes.CngMode = CngChainingMode.Gcm;
                aes.Padding = PaddingMode.None;
                aes.TagSize = 128;


                //byte[] salt = new byte[16];
                //salt = Util.GetRandomNonce(salt);
                //salt = Util.RandomSalt(salt);
                //byte[] salt = new byte[] { 10, 20, 30, 40, 50, 60, 70, 80, 10, 20, 30, 40, 50, 60, 70, 20 };
                //Console.WriteLine(BitConverter.ToString(salt));
                // Keys work the same as standard AES
                //byte[] key = Util.CreateKey(base64key, salt);

                aes.Key = key;




                // The IV (called the nonce in many of the authenticated algorithm specs) is not sized for

                // the input block size. Instead its size depends upon the algorithm. 12 bytes works

                // for both GCM and CCM. Generate a random 12 byte nonce here.

                byte[] nonce = new byte[12];
                nonce = RandomSalt(nonce);
                //byte[] nonce = new byte[] { 10, 20, 30, 40, 50, 60, 70, 80, 10, 20, 30, 40 };

                aes.IV = nonce;



                // Authenticated data becomes part of the authentication tag that is generated during

                // encryption, however it is not part of the ciphertext. That is, when decrypting the

                // ciphertext the authenticated data will not be produced. However, if the

                // authenticated data does not match at encryption and decryption time, the

                // authentication tag will not validate.
                // byte[] tagByte = new byte[16];
                //tagByte = Util.GetRandomNonce(tagByte);
                //      byte[] tagByte = new byte[] { 10, 20, 30, 40, 50, 60, 70, 80, 10, 20, 30, 40, 10, 20, 30, 40 };
                //    aes.AuthenticatedData = tagByte;



                // Perform the encryption - this works nearly the same as standard symmetric encryption,

                // however instead of using an ICryptoTransform we use an IAuthenticatedCryptoTrasform

                // which provides access to the authentication tag.

                using (MemoryStream ms = new MemoryStream())

                using (IAuthenticatedCryptoTransform encryptor = aes.CreateAuthenticatedEncryptor())

                using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))

                {

                    // Encrypt the secret message

                    byte[] plaintext = Encoding.UTF8.GetBytes(textToEncrypt);

                    cs.Write(plaintext, 0, plaintext.Length);



                    // Finish the encryption and get the output authentication tag and ciphertext

                    cs.FlushFinalBlock();
                    Console.WriteLine("ll");
                    byte[] tag = encryptor.GetTag();
                    byte[] ciphertext = ms.ToArray();
                    byte[] ct = new byte[tag.Length + ciphertext.Length];
                    ciphertext.CopyTo(ct, 0);
                    tag.CopyTo(ct, ciphertext.Length);
                    Console.WriteLine("KEY : " + Base64Encode(key));
                    Console.WriteLine("IV : " + Base64Encode(nonce));
                    //Console.WriteLine("SALT : " + Util.Base64Encode((salt)));
                    Console.WriteLine("Tag : " + Base64Encode((tag)));
                    Console.WriteLine("CipherText : " + Base64Encode((ciphertext)));
                    int mergeLength = nonce.Length + ciphertext.Length + tag.Length;
                    Console.WriteLine(mergeLength);
                    byte[] final = new byte[mergeLength];
                    nonce.CopyTo(final, 0);
                    //salt.CopyTo(final, nonce.Length);
                    ciphertext.CopyTo(final, nonce.Length);
                    tag.CopyTo(final, nonce.Length + ciphertext.Length);
                    Console.WriteLine("CipherText + Tag: " + Base64Encode((ct)));

                    var base64Encodedfinal = Base64Encode((final));
                    Console.WriteLine(base64Encodedfinal);
                    return base64Encodedfinal;

                }
            }
        }

        public static string decrypt(string toDecrypt, string base64key)
        {
            // To decrypt, we need to know the nonce, key, additional authenticated data, and

            // authentication tag.
            byte[] str = Base64Decode(toDecrypt);
            using (AuthenticatedAesCng aes = new AuthenticatedAesCng())

            {

                // Chaining modes, keys, and IVs must match between encryption and decryption

                aes.CngMode = CngChainingMode.Gcm;
                //byte[] salt = new byte[16];
                //Array.Copy(str, 12, salt, 0, salt.Length);

                // Console.WriteLine(BitConverter.ToString(salt));
                // Keys work the same as standard AES
                //byte[] key = Util.CreateKey(base64key, salt);
                byte[] key = Convert.FromBase64String(base64key);

                aes.Key = key;
                byte[] iv = new byte[12];
                Array.Copy(str, 0, iv, 0, 12);
                aes.IV = iv;





                // The tag that was generated during encryption gets set here as input to the decryption

                // operation. This is in contrast to the encryption code path which does not use the

                // Tag property (since it is an output from encryption).
                byte[] tag = new byte[16];
                Array.Copy(str, str.Length - 16, tag, 0, 16);
                aes.Tag = tag;



                // Decryption works the same as standard symmetric encryption

                using (MemoryStream ms = new MemoryStream())

                using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))

                {
                    byte[] ciphertext = new byte[str.Length - (iv.Length + tag.Length)];
                    Array.Copy(str, iv.Length, ciphertext, 0, ciphertext.Length);

                    cs.Write(ciphertext, 0, ciphertext.Length);



                    // If the authentication tag does not match, we'll fail here with a

                    // CryptographicException, and the ciphertext will not be decrypted.

                    cs.FlushFinalBlock();



                    byte[] plaintext = ms.ToArray();
                    var decryptedText = Encoding.UTF8.GetString(plaintext);

                    Console.WriteLine("Decrypted and authenticated message: {0}", decryptedText);
                    return decryptedText;

                }

            }
        }

        internal  static  byte[] RandomSalt(byte[] calt)
        {
            for (int i = 0; i < calt.Length; i++)
            {
                calt[i] = (byte)RandomNumber(1, 100);
            }
            return calt;
        }

        private static readonly Random _random = new Random();

        // Generates a random number within a range.      
        public static int RandomNumber(int min, int max)
        {
            return _random.Next(min, max);
        }

        internal static string Base64Encode(byte[] v)
        {

            return System.Convert.ToBase64String(v);
        }
        internal static byte[] Base64Decode(string base64EncodedData)
        {
            return System.Convert.FromBase64String(base64EncodedData);

        }
    }
}
