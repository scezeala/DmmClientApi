﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DMMClient
{
    public class RequestModel
    {
        [JsonProperty("affiliateCode")]
        public  string AffiliateCode { get; set; }
        [JsonProperty("claims")]
        public List<RequestClaims> Claims { get; set; }
        [JsonProperty("count")]
        public int Count { get; set; }

    }

    public class RequestClaims
    {
        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }
        [JsonProperty("claimAmount")]
        public decimal Amount { get; set; }
        [JsonProperty("transDate")]
        public DateTime TransDate { get; set; }
        [JsonProperty("transId")]
        public string TransId { get; set; }
        [JsonProperty("transCategory")]
        public string TransCategory { get; set; }
        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }
        [JsonProperty("comments")]
        public string Comment { get; set; }
    }


    public class Token
    {
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("count")]
        public string message { get; set; }
        [JsonProperty("data")]
        public TokeData Data { get; set; }

    }

    public class TokeData
    {
        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }
        [JsonProperty("tokenType")]
        public string TokenType { get; set; }
        [JsonProperty("expiresIn")]
        public int Expiry { get; set; }
    }
}
